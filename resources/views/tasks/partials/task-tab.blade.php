<div class="tab-pane {{{ $status or '' }}}" id="{{ $tab }}">
    <h1 class="tab-title">
        {{ $title }}
    </h1>

    <div class="table-responsive">
        <table class="table table-striped task-table table-condensed">
            <thead>
                <th>ID</th>
                <th>Nombre</th>
                <th>Descripciòn</th>
                <th>Prioridad</th>
                <th>Fecha de vencimiento</th>
                <th colspan="3">Estado</th>
            </thead>
            <tbody>
                @foreach ($tasks as $task)
                    @include('tasks.partials.task-row')
                @endforeach
            </tbody>
        </table>
    </div>
</div>